package net.floatinginspace.androidnotification.ui

import com.intellij.openapi.project.Project
import com.intellij.openapi.wm.ToolWindow
import com.intellij.openapi.wm.ToolWindowFactory
import com.intellij.ui.content.Content
import com.intellij.ui.content.ContentFactory
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers

class PusherToolWindow : ToolWindowFactory {
    override fun createToolWindowContent(project: Project, toolWindow: ToolWindow) {
        val scope = CoroutineScope(Dispatchers.Main)
        val content: Content =
            ContentFactory.getInstance().createContent(createPusherContent(project, scope), "", false)
        toolWindow.contentManager.addContent(content)
    }
}
