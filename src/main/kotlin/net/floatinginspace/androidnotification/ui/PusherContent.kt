package net.floatinginspace.androidnotification.ui

import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.DialogPanel
import com.intellij.ui.components.JBScrollPane
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import net.floatinginspace.androidnotification.android.UnsubscribeDevices
import net.floatinginspace.androidnotification.android.getAndroidAppId
import net.floatinginspace.androidnotification.android.getAndroidDevices
import net.floatinginspace.androidnotification.android.subscribeDevices
import java.awt.BorderLayout
import javax.swing.BorderFactory
import javax.swing.JComponent
import javax.swing.JPanel

fun createPusherContent(project: Project, scope: CoroutineScope) = JPanel().apply {
    layout = BorderLayout()
    scope.launch(Dispatchers.Main) {
        val model = PusherModel(
            appId = getAndroidAppId(project) ?: ""
        ).withDevices(getAndroidDevices(project) ?: emptyArray())
        val content = JPanel().apply {
            withPadding()
            add(render(scope, this, model, project))
        }
        scrollPane(content)
    }
}

private const val PADDING_VERTICAL = 10
private const val PADDING_HORIZONTAL = 20
private fun JComponent.withPadding() = apply {
    layout = BorderLayout()
    border = BorderFactory.createEmptyBorder(PADDING_VERTICAL, PADDING_HORIZONTAL, PADDING_VERTICAL, PADDING_HORIZONTAL)
}

private fun JComponent.scrollPane(component: JComponent) = this.add(
    JBScrollPane(component),
    BorderLayout.CENTER
)

private suspend fun render(scope: CoroutineScope, root: JPanel, model: PusherModel, project: Project): JComponent {
    lateinit var unsubscribeDevices: UnsubscribeDevices
    lateinit var panel: DialogPanel

    val reRender: (() -> PusherModel) -> Unit = { createModel ->
        scope.launch(Dispatchers.Main) {
            unsubscribeDevices()
            panel.updateModel()
            root.apply {
                removeAll()
                add(render(scope, this, createModel(), project))
                revalidate()
                repaint()
            }
        }
    }

    subscribeDevices(scope, project) { reRender { model.withDevices(it) } }
        .also { unsubscribeDevices = it }

    panel = createPusherView(
        model = model,
        onAddKeyValue = { reRender { model.withNewKeyValue() } },
        onDeleteKeyValue = { reRender { model.withoutKeyValue(it) } },
        onSend = { panel.updateModel().also { scope.launch { onSendPush(model, reRender) } } }
    )

    return panel
}

// Convenience function to avoid confusion Kotlin's `apply` scope functions
// This should be called anytime that the model needs to be read (such as when re-rendering)
private fun DialogPanel.updateModel() = this.apply()
