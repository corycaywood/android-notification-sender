package net.floatinginspace.androidnotification.ui

import com.android.ddmlib.IDevice
import net.floatinginspace.androidnotification.common.MutablePair
import net.floatinginspace.androidnotification.common.mutableTo

typealias AppId = String
typealias KeyValues = List<MutablePair<String, String>>
typealias Devices = List<MutablePair<IDevice, Boolean>>

data class PusherModel(
    val devices: Devices = emptyList(),
    var appId: AppId = "com.your.package.name",
    val keyValues: KeyValues = listOf("message" mutableTo "content"),
    val errorMessages: List<String> = emptyList(),
    val successMessages: List<String> = emptyList()
) {
    fun withNewKeyValue(new: MutablePair<String, String> = "" mutableTo "") =
        copy(keyValues = keyValues + new)

    fun withoutKeyValue(minus: MutablePair<String, String>) = copy(keyValues = keyValues - minus)

    fun withDevices(newDevices: Array<IDevice>?) = copy(
        devices = newDevices?.createDeviceCheckboxes(devices) ?: emptyList()
    )

    fun withError(message: String) = copy(errorMessages = listOf(message))

    fun withSuccess(message: String) = copy(successMessages = listOf(message))
}

val Devices.selected get() = filter { it.second }
    .map { it.first }

fun KeyValues.toPairs() = map { it.first to it.second }

val KeyValues.isBlank get() = filterNot { it.first.isBlank() || it.second.isBlank() }.isEmpty()

val AppId.isInvalid get() = all { it.isDigit() || it.isLetter() || it == '_' || it == '.' }.not()
    .or(none { it == '.' })

private fun Array<IDevice>.createDeviceCheckboxes(old: Devices) = this.map { device ->
    device mutableTo old.any { it.first.serialNumber == device.serialNumber && it.second }
}
