package net.floatinginspace.androidnotification.ui

import net.floatinginspace.androidnotification.android.SendNotificationAction
import net.floatinginspace.androidnotification.android.SendResult
import net.floatinginspace.androidnotification.android.sendNotification

suspend fun onSendPush(
    model: PusherModel,
    onResult: (createModel: () -> PusherModel) -> Unit,
    send: SendNotificationAction = sendNotification
) {
    val newModel = model.copy(errorMessages = emptyList(), successMessages = emptyList())

    when {
        newModel.keyValues.isBlank -> ERROR_KEY_VALUES_BLANK
        newModel.appId.isInvalid -> ERROR_APP_ID_INVALID
        newModel.devices.selected.isEmpty() -> ERROR_NO_SELECTED_DEVICES
        else -> null
    }?.let {
        return onResult { newModel.withError(it) }
    }

    val successCollector: MutableList<String> = mutableListOf()
    val errorCollector: MutableList<String> = mutableListOf()
    for (device in newModel.devices.selected) {
        send(device, newModel.appId, newModel.keyValues.toPairs()).let {
            when(it) {
                is SendResult.Success -> successCollector += it.message
                is SendResult.Error -> errorCollector += it.error
            }
        }
    }

    onResult { newModel.copy(errorMessages = errorCollector, successMessages = successCollector) }
}

const val ERROR_KEY_VALUES_BLANK = "You must define at least one non-empty Key/Value pair."
const val ERROR_APP_ID_INVALID =  "The App ID must be a valid package name."
const val ERROR_NO_SELECTED_DEVICES = "You must select at least one device to send the notification to."
