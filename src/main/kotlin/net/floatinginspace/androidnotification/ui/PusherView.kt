@file:Suppress("WildcardImport")
package net.floatinginspace.androidnotification.ui

import com.android.ddmlib.IDevice
import com.intellij.icons.AllIcons
import com.intellij.ui.dsl.builder.*
import net.floatinginspace.androidnotification.common.MutablePair
import javax.swing.Icon

fun createPusherView(
    model: PusherModel,
    onAddKeyValue: () -> Unit,
    onDeleteKeyValue: (MutablePair<String, String>) -> Unit,
    onSend: () -> Unit
)  = panel {
    group("Devices") {
        createCheckboxes(model.devices)
    }.apply { bottomGap(BottomGap.MEDIUM) }

    group("App ID") {
        row {
            textField().bindText(model::appId)
        }
    }.apply { bottomGap(BottomGap.MEDIUM) }

    group("String Extras") {
        createKeyValues(
            keyValues = model.keyValues,
            onClickAdd = onAddKeyValue,
            onClickDelete = onDeleteKeyValue
        )
    }.apply { bottomGap(BottomGap.MEDIUM) }

    row {
        button("Send") { onSend() }.apply { align(Align.FILL) }
    }.apply { bottomGap(BottomGap.SMALL) }

    model.errorMessages.forEach { infoText(it, AllIcons.General.Error) }
    model.successMessages.forEach { infoText(it, AllIcons.General.InspectionsOK) }
}

private fun Panel.createCheckboxes(checkBoxes: Devices)  {
    if (checkBoxes.isNotEmpty()) {
        row {
            for (checkBox in checkBoxes) {
                checkBox(checkBox.first.name).bindSelected(checkBox::second)
            }
        }
    }
}

private fun Panel.createKeyValues(
    keyValues: List<MutablePair<String, String>>,
    onClickAdd: () -> Unit,
    onClickDelete: (MutablePair<String, String>) -> Unit
) {
    for (keyValue in keyValues) {
        keyValueRow(keyValue) {
            onClickDelete(keyValue)
        }
    }
    panel {
        row{
            button("Add +") { onClickAdd() }.apply { align(Align.FILL) }
        }
    }
}

private fun Panel.keyValueRow(pair: MutablePair<String, String>, onClickDelete: () -> Unit) = row {
    label("Key:")
    textField().bindText(pair::first)
    label("  Value:")
    textField().bindText(pair::second)
    button("Remove -") { onClickDelete() }
}

private fun Panel.infoText(text: String, icon: Icon) = row {
    icon(icon)
    text("""<html>$text</html>""")
}
