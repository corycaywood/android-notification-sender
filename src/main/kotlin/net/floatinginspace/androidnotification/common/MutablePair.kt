package net.floatinginspace.androidnotification.common

data class MutablePair<A, B>(var first: A, var second: B)

infix fun <A, B> A.mutableTo(that: B): MutablePair<A, B> = MutablePair(this, that)
