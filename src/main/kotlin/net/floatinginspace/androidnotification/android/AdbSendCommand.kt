package net.floatinginspace.androidnotification.android

import com.android.ddmlib.IDevice
import com.android.ddmlib.IShellOutputReceiver
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

sealed interface SendResult {
    data class Success(val message: String): SendResult
    data class Error(val error: String): SendResult
}

private const val DELAY_FOR_GETTING_ROOT = 1500L
typealias SendNotificationAction = suspend (IDevice, String, List<Pair<String, String>>) -> SendResult
val sendNotification: SendNotificationAction = { device, appId, stringExtras ->
    withContext(Dispatchers.Default) {
        if (device.isEmulator && !device.isRoot) {
            // It takes a second for ADB to reload as root, but there's no callback for when it succeeds
            // So we just wait a little bit after requesting root()
            device.root()
            delay(DELAY_FOR_GETTING_ROOT)
        }

        val result = suspendCoroutine { continuation ->
            device.executeShellCommand(
                createSendCommand(appId, stringExtras),
                createShellOutputReceiver {
                    continuation.resume(createSendResult(it, device))
                }
            )
        }

        return@withContext result
    }
}

fun createSendCommand(appId: String, stringExtras: List<Pair<String, String>>) =
    createExtrasParam(stringExtras).let { extras ->
        "am broadcast" +
            " -n $appId/com.google.firebase.iid.FirebaseInstanceIdReceiver" +
            " -a \"com.google.android.c2dm.intent.RECEIVE\"" +
            " $extras"
    }

fun createShellOutputReceiver(onFinished: (String?) -> Unit) = object : IShellOutputReceiver {

    val messages: MutableList<String> = mutableListOf()
    override fun addOutput(data: ByteArray, offset: Int, length: Int) {
        messages += data.toString(Charsets.UTF_8)
    }

    override fun flush() { onFinished(parseResultCode(messages.toList())) }

    override fun isCancelled() = false
}

private const val URL_USER_GUIDE =
    "https://gitlab.com/corycaywood/android-notification-pusher#testing-on-non-rooted-devices"
private const val RESULT_OK = "-1"
fun createSendResult(result: String?, device: IDevice) = when(result) {
    RESULT_OK -> SendResult.Success("${device.name}: Notification successfully sent.")
    else -> SendResult.Error(
        "${device.name}: Failed to send the notification. <br>" +
                when {
                    device.isEmulator -> "When sending to an emulator that has Play Services installed, "
                    else -> "When sending to a real device (not an emulator), "
                } +
                "you must perform some additional <br>" +
                "setup in AndroidManifest.xml. Please see the <a href=\"$URL_USER_GUIDE\">user guide</a> " +
                "of this plugin for more information."
    )
}

private fun createExtrasParam(stringExtras: List<Pair<String, String>>) =
    stringExtras.filterNot { it.first.isBlank() || it.second.isBlank() }
        .map { it.first.escapeQuotes() to it.second.escapeQuotes() }
        .joinToString("") { """ --es "${it.first}" "${it.second}" """ }

private fun String.escapeQuotes() = replace("\"", "\\\"")

private const val BROADCAST_COMPLETED_REGEX = "[\\s\\S]*?(Broadcast completed: result=)(-?[0-9]*)[\\s\\S]*?"
private fun parseResultCode(messages: List<String>) = messages.run {
    val regex = Regex(BROADCAST_COMPLETED_REGEX)
    find { it.contains(regex) }?.let {
        regex.matchEntire(it)?.destructured?.component2()
    }
}
