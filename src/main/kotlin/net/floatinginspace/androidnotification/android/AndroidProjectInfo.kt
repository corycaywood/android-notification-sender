package net.floatinginspace.androidnotification.android

import com.intellij.openapi.module.ModuleManager
import com.intellij.openapi.project.Project
import org.jetbrains.android.dom.manifest.Manifest
import org.jetbrains.android.facet.AndroidFacet
import org.jetbrains.android.facet.AndroidRootUtil
import org.jetbrains.android.util.AndroidUtils

fun getAndroidAppId(
    project: Project,
    moduleManager: ModuleManager = ModuleManager.getInstance(project),
    androidFacet: AndroidFacet? = moduleManager.modules.firstNotNullOfOrNull { AndroidFacet.getInstance(it) }
) = androidFacet?.let { facet ->
        AndroidRootUtil.getPrimaryManifestFile(facet)?.let {
            val manifest = AndroidUtils.loadDomElement(facet.module, it, Manifest::class.java)
            manifest?.`package`?.value
        }
    }
