package net.floatinginspace.androidnotification.android

import com.android.ddmlib.AndroidDebugBridge
import com.android.ddmlib.IDevice
import com.intellij.openapi.project.Project
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.jetbrains.android.sdk.AndroidSdkUtils

fun getAndroidDevices(project: Project) = AndroidSdkUtils.getDebugBridge(project)?.devices

typealias UnsubscribeDevices = () -> Unit
fun subscribeDevices(
    scope: CoroutineScope,
    project: Project,
    onDevicesChanged: (Array<IDevice>?) -> Unit
) : UnsubscribeDevices {
    val launchOnDevicesChanged = {
        scope.launch(Dispatchers.Main) {
            onDevicesChanged(getAndroidDevices(project))
        }
    }

    val listener = object : AndroidDebugBridge.IDeviceChangeListener {
        override fun deviceConnected(device: IDevice?) { launchOnDevicesChanged() }
        override fun deviceDisconnected(device: IDevice?) { launchOnDevicesChanged() }
        override fun deviceChanged(device: IDevice?, changeMask: Int) { /* Do nothing */ }
    }
    AndroidDebugBridge.addDeviceChangeListener(listener)

    return { AndroidDebugBridge.removeDeviceChangeListener(listener) }
}
