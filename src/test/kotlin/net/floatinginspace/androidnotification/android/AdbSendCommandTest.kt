package net.floatinginspace.androidnotification.android

import org.amshove.kluent.shouldBeEqualTo
import org.amshove.kluent.shouldContain
import org.amshove.kluent.shouldStartWith
import org.junit.jupiter.api.Test

class AdbSendCommandTest {

    @Test
    fun `createSendCommand should create command for broadcasting intent`() {
        val appId = "com.package.test"
        val extras = listOf("test-key" to "test-value")

        val command = createSendCommand(appId, extras)
        command shouldStartWith "am broadcast"
        command shouldContain " -a \"com.google.android.c2dm.intent.RECEIVE\""
    }

    @Test
    fun `createSendCommand should create command with correct app id`() {
        val appId = "com.package.test"
        val extras = listOf("test-key" to "test-value")

        createSendCommand(appId, extras) shouldContain "-n $appId/com.google.firebase.iid.FirebaseInstanceIdReceiver"
    }

    @Test
    fun `createSendCommand should create command with correct string extras"`() {
        val appId = "com.package.test"
        val extras = listOf("test-key" to "test-value", "test-key-2" to "test-value-2")

        val command = createSendCommand(appId, extras)
        command shouldContain "--es \"test-key\" \"test-value\""
        command shouldContain "--es \"test-key-2\" \"test-value-2\""
    }

    @Test
    fun `createSendCommand should escape quotation marks in string extras"`() {
        val appId = "com.package.test"
        val extras = listOf("\"test-key\"" to "{\"test-json-key\": \"test-json-value\"}")

        val command = createSendCommand(appId, extras)
        command shouldContain """ "\"test-key\"" """.trim()
        command shouldContain """ "{\"test-json-key\": \"test-json-value\"}" """.trim()
    }

    @Test
    fun `createShellOutputReceiver should return shell result code to callback`() {
        val message1 = "Broadcasting: Intent { act=com.google.android.c2dm.intent.RECEIVE flg=0x400000 " +
                "cmp=com.package.test/com.google.firebase.iid.FirebaseInstanceIdReceiver (has extras) }"
        val message2 = "Broadcast completed: result=-1\ngoogle.android.c2dm.intent.RECEIVE flg=0x400000 " +
                "cmp=com.package.test/com.google.firebase.iid.FirebaseInstanceIdReceiver (has extras) }"

        var result: String? = "none"
        shellOutputReceiverWithMessages(message1, message2) { result = it }

        result shouldBeEqualTo "-1"
    }

    @Test
    fun `createShellOutputReceiver should return null result when messages are missing result code`() {
        val message1 = "first incorrect message"
        val message2 = "second incorrect message"

        var result: String? = "none"
        shellOutputReceiverWithMessages(message1, message2) { result = it }

        result shouldBeEqualTo null
    }

    private fun shellOutputReceiverWithMessages(vararg messages: String, result: (String?) -> Unit) {
        val receiver = createShellOutputReceiver(result)
        messages.forEach { receiver.addOutput(it.toByteArray(Charsets.UTF_8), 0, it.length) }
        receiver.flush()
    }
}
