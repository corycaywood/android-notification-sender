package net.floatinginspace.androidnotification.ui

import com.android.ddmlib.IDevice
import net.floatinginspace.androidnotification.common.mutableTo
import org.amshove.kluent.*
import org.mockito.kotlin.mock
import kotlin.test.Test

class PusherModelTest {

    @Test
    fun `KeyValue isBlank should be false when there is a valid KeyValue`() {
        val model = PusherModel(keyValues = listOf("test-key" mutableTo "test-value", "" mutableTo ""))

        model.keyValues.isBlank shouldBe false
    }

    @Test
    fun `KeyValue isBlank should be true when there is only blank key values`() {
        val model = PusherModel(keyValues = listOf("" mutableTo "", " " mutableTo "   "))
        model.keyValues.isBlank shouldBe true
    }

    @Test
    fun `withNewKeyValue should add an empty KeyValue by default`() {
        val model = PusherModel(keyValues = listOf("test-key" mutableTo "test-value"))
        model.withNewKeyValue().keyValues.last() shouldBeEqualTo ("" mutableTo "")
    }

    @Test
    fun `withNewKeyValue should add a new KeyValue`() {
        val model = PusherModel(keyValues = listOf("test-key" mutableTo "test-value"))
        val newKeyValue = "new-key" mutableTo "new-value"

        model.withNewKeyValue(newKeyValue).keyValues.last() shouldBeEqualTo (newKeyValue)
    }

    @Test
    fun `withNewKeyValue should still keep the original KeyValues`() {
        val oldKeyValue = "test-key" mutableTo "test-value"
        val model = PusherModel(keyValues = listOf(oldKeyValue))

        model.withNewKeyValue().keyValues.first() shouldBeEqualTo (oldKeyValue)
    }

    @Test
    fun `withoutKeyValue should remove the specified KeyValue`() {
        val keyValue = "test-key" mutableTo "test-value"
        val model = PusherModel(keyValues = listOf(keyValue))

        keyValue shouldNotBeIn model.withoutKeyValue(keyValue).keyValues
    }

    @Test
    fun `withoutKeyValue should not remove any other KeyValue`() {
        val keyValue = "test-key" mutableTo "test-value"
        val removedKeyValue = "test-key2" mutableTo "test-value2"
        val model = PusherModel(keyValues = listOf(keyValue, removedKeyValue))

        keyValue shouldBeIn model.withoutKeyValue(removedKeyValue).keyValues
    }

    @Test
    fun `selected devices should return devices which are selected`() {
        val selectedDevice: IDevice = mock()
        val unselectedDevice: IDevice = mock()
        val model = PusherModel(devices = listOf(selectedDevice mutableTo true, unselectedDevice mutableTo false))

        model.devices.selected shouldBeEqualTo listOf(selectedDevice)
    }

    @Test
    fun `selected devices should not return devices which are not selected`() {
        val selectedDevice: IDevice = mock()
        val unselectedDevice: IDevice = mock()
        val model = PusherModel(devices = listOf(selectedDevice mutableTo true, unselectedDevice mutableTo false))

        unselectedDevice shouldNotBeIn model.devices.selected
    }

    @Test
    fun `withDevices should create devices pair list with false default selected state`() {
        val device: IDevice = mock()
        val model = PusherModel()

        model.withDevices(arrayOf(device)).devices.first().second shouldBe false
    }

    @Test
    fun `withDevices should maintain the selected state of previous devices list`() {
        val device1: IDevice = mock { val serialNumber = "device1" }
        val device2: IDevice = mock { val serialNumber = "device2" }
        val previousDevice = device1 mutableTo true
        val model = PusherModel(devices = listOf(previousDevice))

        model.withDevices(arrayOf(device1, device2)).devices
            .find { it.first.serialNumber == device1.serialNumber }!!.second shouldBe true
    }

    @Test
    fun `withError should create list with single error message`() {
        val model = PusherModel()
        model.withError("error").errorMessages.first() shouldBeEqualTo "error"
    }

    @Test
    fun `withSuccess should create list with single success message`() {
        val model = PusherModel()
        model.withSuccess("success").successMessages.first() shouldBeEqualTo "success"
    }

    @Test
    fun `AppId isInvalid should return false when AppId is valid`(){
        val model = PusherModel(appId = "com.package._test")
        model.appId.isInvalid shouldBe false
    }

    @Test
    fun `AppId isInvalid should return true when AppId contains spaces`(){
        val model = PusherModel(appId = "com.package. test")
        model.appId.isInvalid shouldBe true
    }

    @Test
    fun `AppId isInvalid should return true when AppId contains symbols`(){
        val model = PusherModel(appId = "com.package.-test()$%+=")
        model.appId.isInvalid shouldBe true
    }

    @Test
    fun `AppId isInvalid should return true when AppId is blank`(){
        val model = PusherModel(appId = "")
        model.appId.isInvalid shouldBe true
    }
}
