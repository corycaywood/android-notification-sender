package net.floatinginspace.androidnotification.ui

import com.android.ddmlib.IDevice
import kotlinx.coroutines.test.runTest
import net.floatinginspace.androidnotification.android.SendNotificationAction
import net.floatinginspace.androidnotification.android.SendResult
import net.floatinginspace.androidnotification.common.mutableTo
import org.amshove.kluent.shouldBeEqualTo
import org.amshove.kluent.shouldContain
import org.junit.jupiter.api.Test

import org.mockito.Mockito.mock

class SendPushActionTest {

    @Test
    fun `onSend should return error when keyValues are blank`() = runTest {
        lateinit var result: PusherModel
        val model = PusherModel(keyValues = listOf(" " mutableTo " "))

        onSendPush(model, createOnResult { result = it }, mock())

        result.errorMessages shouldContain ERROR_KEY_VALUES_BLANK
    }

    @Test
    fun `onSend should return error when AppId is invalid`() = runTest {
        lateinit var result: PusherModel
        val model = PusherModel(appId = "com-package.test*&^")

        onSendPush(model, createOnResult { result = it }, mock())

        result.errorMessages shouldContain ERROR_APP_ID_INVALID
    }

    @Test
    fun `onSend should return error when no devices are selected`() = runTest {
        lateinit var result: PusherModel
        val device: IDevice = mock()
        val model = PusherModel(devices = listOf(device mutableTo false))

        onSendPush(model, createOnResult { result = it }, mock())

        result.errorMessages shouldContain ERROR_NO_SELECTED_DEVICES
    }

    @Test
    fun `onSend should return success messages when notification was sent successfully`() = runTest {
        lateinit var result: PusherModel
        val device: IDevice = mock()
        val successMessage = "success"
        val onSend: SendNotificationAction = { _, _, _ -> SendResult.Success(successMessage) }
        val model = PusherModel(devices = listOf(device mutableTo true, device mutableTo true))

        onSendPush(model, createOnResult { result = it }, onSend)

        result.successMessages shouldBeEqualTo listOf(successMessage, successMessage)
    }

    @Test
    fun `onSend should return error messages when notification failed to send`() = runTest {
        lateinit var result: PusherModel
        val device: IDevice = mock()
        val errorMessage = "error"
        val onSend: SendNotificationAction = { _, _, _ -> SendResult.Error(errorMessage) }
        val model = PusherModel(devices = listOf(device mutableTo true, device mutableTo true))

        onSendPush(model, createOnResult { result = it }, onSend)

        result.errorMessages shouldBeEqualTo listOf(errorMessage, errorMessage)
    }

    private fun createOnResult(onResult: (PusherModel) -> Unit) = 
        { createModel: () -> PusherModel -> onResult(createModel()) }
}
