# Android Notification Pusher
[Plugin for Android Studio and IntelliJ Idea](https://plugins.jetbrains.com/plugin/23799-android-notification-pusher) which allows you to send push notifications directly to your Android device without any need for using the Firebase Console.

![screenshot](./images/screenshot.png)

- Simultaneously send push messages to multiple devices and emulators.
- Add as many key/value pairs as you need.
- No need to use the Firebase Console to test a push message. Messages are sent directly using ADB.

**Note:** If you wish to test on a non-rooted device (or an emulator with Play Services installed), you must do some additional setup. Please see the Getting Started instructions.

## Getting Started

This Plugin will be added as a tool window named "Notification Pusher", and it will initially show up in your right side tool panels.

1. Your App must be setup to use Firebase Cloud Messaging. See the [Firebase Android setup guide](https://firebase.google.com/docs/cloud-messaging/android/client).
2. Select the devices and emulators you wish to send to.
3. Set the App ID for your App. The "App ID" field will default to the App ID of your current Android project.
4. Enter the key/values you want to send. You must enter at least one non-empty key/value pair.
5. Click "Send" to push the notification to your devices. Success and Error messages will be displayed at the bottom of the panel.

### Testing on non-rooted devices

This plugin works best when used with rooted Android devices or Android emulators which can run as root (AVD images without Google Play).

However, you can also use this plugin on non-rooted devices and any other type of AVD image by adding the following to your `AndroidManifest.xml`

**Note:** This config should **NOT** be added to release builds of your App. You should create a debug config under `app/src/debug/AndroidManifest.xml`.

```xml
<!-- This should be added to your debug config in app/src/debug/AndroidManifest.xml -->
<!-- Be sure to replace "com.package.name" with your own App ID -->
<manifest xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools">
    <application >
        <receiver
            android:name="com.google.firebase.iid.FirebaseInstanceIdReceiver"
            android:exported="true"
            android:permission="@null"
            tools:replace="android:permission">
            <intent-filter>
                <action android:name="com.google.android.c2dm.intent.RECEIVE" />
                <category android:name="com.package.name" />
            </intent-filter>
        </receiver>
    </application>
</manifest>
```

## Releases

See [CHANGELOG](CHANGELOG.md)
