import io.gitlab.arturbosch.detekt.Detekt

plugins {
  id("java")
  id("org.jetbrains.kotlin.jvm") version "1.9.21"
  id("org.jetbrains.intellij") version "1.16.1"
  id("io.gitlab.arturbosch.detekt") version "1.23.5"
}

group = "net.floatinginspace"
version = "1.0.1"

repositories {
  mavenCentral()
}

dependencies {
  testImplementation(kotlin("test"))
  testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-test:1.8.0")
  testImplementation("org.mockito:mockito-core:5.2.1")
  testImplementation("org.mockito.kotlin:mockito-kotlin:5.2.1")
  testImplementation("org.amshove.kluent:kluent:1.73")
}

// Configure Gradle IntelliJ Plugin
// Read more: https://plugins.jetbrains.com/docs/intellij/tools-gradle-intellij-plugin.html
intellij {
  version.set("231.7864.76.2311.10195651")
  type.set("AI")

  plugins.set(listOf("android"))
}

detekt {
  buildUponDefaultConfig = true
  allRules = false
  config.setFrom("$projectDir/detekt.yml")
}

tasks {
  withType<JavaCompile> {
    sourceCompatibility = "17"
    targetCompatibility = "17"
  }
  withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions.jvmTarget = "17"
  }

  patchPluginXml {
    sinceBuild.set("231")
    untilBuild.set("241.*")
  }

  val chainFile = file("signing/chain.crt")
  val privateKeyFile = file("signing/private.pem")
  val passwordFile = file("signing/password.txt")
  if (chainFile.exists() && privateKeyFile.exists() && passwordFile.exists()) {
    signPlugin {
      certificateChain.set(chainFile.readText())
      privateKey.set(privateKeyFile.readText())
      password.set(passwordFile.readText())
    }
  }

  val tokenFile = file("signing/token.txt")
  if (tokenFile.exists()) {
    publishPlugin {
      token.set(tokenFile.readText())
    }
  }

  // Not needed because this plugin doesn't use settings
  // see https://github.com/JetBrains/gradle-intellij-plugin/blob/b21e3f382e9885948a6427001d5e64234c602613/src/main/kotlin/org/jetbrains/intellij/tasks/BuildSearchableOptionsTask.kt#L20
  buildSearchableOptions {
    enabled = false
  }

  test {
    useJUnitPlatform()
  }

  withType<Detekt>().configureEach {
    jvmTarget = JavaVersion.VERSION_17.toString()
  }
}
