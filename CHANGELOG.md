# Releases

## 1.0.1 (Feb 29, 2024)
- Fixed the plugin icons so that they are not blurry when displayed.

## 1.0.0 (Feb 20, 2024)
- Initial release
